# LOE 11ty Theme v1.3.4
This is a a set of default includes and sass for use with Logic of English microsites

## Installation 
```bash
npm install git+https://loethan@bitbucket.org/loewebteam/loe-11ty-theme.git
```

will install the loe-11ty-theme

----
## Sass (SCSS) 
folder: _sass

### _faq_block.scss
styling for the Knowledge base block, and questions to accompany a page
### _download-block.scss 
styling for the download icon with the text for the download
### _footer.scss 
the footer block of course
### _link-button.scss 
all of the buttons use this as a base
### _loe-defaults.scss
variables and mixins used throughout
### _mailchimp.scss
used in the footer
### _mobile-menu.scss
the full-screen menu used for tablet/mobile widths
### _nav-top.scss
white bar at top of each page
### _po-spot.scss
yellow block that links to the purchase order website
### _responsive-media.scss
responsive iframe, video holder, etc.
### _set-contents.scss
should probably be overridden, basic styling for the teacher and student items at the bottom of many product pages.
### _site-defaults.scss
starting point for taking variables and applying them to elements (_default is JUST scss variables)
### _sub-nav.scss
blue bar at top of each page.

----
## Assets
folder: assets 
Reference other sass components in the as relative to the _sass folder.
If the item is not available in the local version of the _sass folder, it will look in the loe-11ty-theme _sass folder.


assets/styles:

### common.scss
starting point for items below the fold that could be used across multiple pages

### home.scss
home page css. contains the following items. use this as a basis for page-specific css
```scss
/* home.scss */
@import 'loe-defaults';
@import 'responsive-media';
@import 'link-button';
@import 'list-box';
@import 'faq-block';
@import 'download-block';
```

assets/scripts:

use webpack to build right into the scripts folder if custom javascript is required.

----

## Includes
[buy-now](documentation/buy-now.md)
[download-block](documentation/download-block.md)
[expandi-box](documentation/expandi-box.md)
[header](documentation/header.md)
[hero-text](documentation/hero-text.md)
[info-btn](documentation/info-btn.md)
[lazy-img](documentation/lazy-img.md)
[link-row](documentation/link-row.md)
[list-box](documentation/list-box.md)
[loe-asset-link](documentation/loe-asset-link.md)
[loe-dialog](documentation/loe-dialog.md)
[scroller](documentation/scroller.md)
[section](documentation/section.md)
[teacher-tip](documentation/teacher-tip.md)
[unstyled](documentation/unstyled.md)

## Contributing
After committing any changes, run the bump:major, bump:minor, or bump:patch npm scripts to bump the version, tag the git version, and push to bitbucket

```bash
# for major releases
bump:major

# for minor releases
bump:minor

# for patch releases
bump:patch
```
