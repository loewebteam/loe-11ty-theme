const socialSVG = require('../social/socialsvg.11ty');
const mailChimpEmbed = require('../mailchimp/mail-chimp-embed.11ty');

module.exports = function() {
    let currentYear = new Date();
    currentYear = currentYear.getFullYear();
    return `<div class="site-footer">
        <a href="https://www.logicofenglish.com" class="img-holder">
            <img alt="Logic of English Breve" data-src="https://res.cloudinary.com/logic-of-english/image/upload/loe-breve.svg">
        </a>
        <p class="tag-line">Combining the Science of Reading<br/>with the Joy of Learning</p>
        <div class="social-links">
            <a class="fb" href="https://facebook.com/TheLogicOfEnglish" aria-label="facebook">
                ${socialSVG.facebook}
            </a>
            <a class="twitter" href="https://twitter.com/LogicofEnglish" aria-label="twitter">
                ${socialSVG.twitter}
            </a>
            <a class="youtube" href="https://youtube.com/LogicofEnglish" aria-label="youtube">
                ${socialSVG.youtube}
            </a>
            <a class="instagram" href="https://www.instagram.com/logicofenglish/" aria-label="instagram">
                ${socialSVG.instagram}
            </a>
        </div>
        <div class="footer-nav-links">
            <a href="https://store.logicofenglish.com">Store</a>
            <a href="https://www.logicofenglish.com/about">About</a>
            <a href="https://logicofenglish.idevaffiliate.com/">Affiliates</a>
            <a href="https://www.logicofenglish.com/about/contact-us">Contact Us</a>
        </div>
        <div class="footer-sub-nav-links">
            <a href="https://store.logicofenglish.com/pages/digital-downloads">Digital</a>
            <a href="https://store.logicofenglish.com/pages/return-policy">Returns</a>
            <a href="https://store.logicofenglish.com/pages/privacy-policy">Privacy</a>
            <a href="https://store.logicofenglish.com/pages/terms">TOS</a>
            <a href="https://store.logicofenglish.com/pages/shipping-information">Shipping</a>
        </div>
        ${mailChimpEmbed()}
        <p class="copyright">
            &copy; 2011-${currentYear} Logic of English® is a registered<br/>trademark of Logic of English, Inc.
        </p>
    </div>`;
}