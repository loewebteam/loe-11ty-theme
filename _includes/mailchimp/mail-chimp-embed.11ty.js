module.exports = function() {
return `<link href="//cdn-images.mailchimp.com/embedcode/slim-10_7.css" rel="stylesheet" type="text/css">
<div id="mc_embed_signup" class="mail-chimp-form">
<form action="https://logicofenglish.us4.list-manage.com/subscribe/post?u=b85d96d77edb97eea2a3e9cbf&amp;id=13d0f222c3" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
	<label for="mce-EMAIL">Stay up to date with our LOE newsletter!</label>
	<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Your Email" required />
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_b85d96d77edb97eea2a3e9cbf_13d0f222c3" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Sign Up" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>
</form>
</div>`
}