module.exports = function(data) {

    let generalURL = "https://logicofenglish.zendesk.com/hc/en-us";
    if(data.faqs && data.faqs.generalUrl) {
        generalURL = data.faqs.generalUrl;
    }
    let questionList = '';
    if(data.faqs && data.faqs.questions) {

        questionList = data.faqs.questions.map((faq) => {
            return `<li>
            <div>
                <svg class="icon-q-mark" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg">
                <path d="M512 64C264.8 64 64 264.8 64 512s200.8 448 448 448 448-200.8 448-448S759.2 64 512 64z m32 704h-64v-64h64v64z m11.2-203.2l-5.6 4.8c-3.2 2.4-5.6 8-5.6 12.8v58.4h-64v-58.4c0-24.8 11.2-48 29.6-63.2l5.6-4.8c56-44.8 83.2-68 83.2-108C598.4 358.4 560 320 512 320c-49.6 0-86.4 36.8-86.4 86.4h-64C361.6 322.4 428 256 512 256c83.2 0 150.4 67.2 150.4 150.4 0 72.8-49.6 112.8-107.2 158.4z" />
                </svg>
            </div>
            <a href="${faq.url}">${faq.question}</a>
            </li>`
        });
        questionList = `<ul>${questionList}</ul>`;
    }

return `<div class="faq-block">
    <h3>Curious for more details?</h3>
    <p>Check out our comprehensive FAQ.</p>
    <div>
        <a href="${generalURL}" class="link-btn">Learn More</a>
    </div>
    ${questionList}
    </div>`;
}