module.exports = function({fileName="missing",linkType="undefined"}) {
    if(linkType == "undefined") {
        console.log("linkType is required");
        return `#missing-link-type`
    }

    if(fileName == "undefined") {
        console.log("fileName is required");
        return `#missing-file-name`;
    }

    return `https://assets.logicofenglish.com/${linkType}/${fileName}`;

}
