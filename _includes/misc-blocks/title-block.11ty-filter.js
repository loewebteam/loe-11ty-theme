module.exports = function(title="title",tagline="tagline") {
    return `<div class="header">
    <h2>${title}</h2>
    <p>${tagline}</p>
  </div>`;
}