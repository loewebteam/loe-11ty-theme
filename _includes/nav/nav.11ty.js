const yaml = require('js-yaml');

module.exports = function(data) {
    let json = data["main-navigation"];
    let yamlString = json.data.raw_text.file_body.value[0].text;
    let fullMenu = yaml.safeLoad(yamlString);
    let subMenu = getPageRoot(fullMenu.menuItems, data.parentNavItem);
    if(!subMenu) {
        return 'NO ROOT DEFINED';
    }

    let pageUrl = data.page.url;
    console.log(pageUrl);

    function getPageRoot(menuArray, nodeId) {
        let retItem = null;
        for(let i=0;i<menuArray.length;i++) {
            let menuItem = menuArray[i];
            if(menuItem.id == nodeId) {
                return menuItem;
            } 
            else if (menuItem.children && menuItem.children.length > 0) {
                retItem = getPageRoot(menuItem.children, nodeId);
                if(retItem) {
                    return retItem;
                }
            }
        }
    }
    
    function entireNav(navObj) {
    
        return 'nav-holder';
    
    }
    
    function mainNav(linkObj) {
        if(!linkObj.children) {
            return ``;
        }
        let subLinks = '';
        for(let i=0;i<linkObj.children.length;i++) {
            let kid = linkObj.children[i];
           
            let activeClass = '';
            if(kid.url + '/' == data.site.baseUrl + pageUrl) {
                activeClass = ` class="active" `
            }
            subLinks += `<a href="${kid.url}"${activeClass}>${kid.title}</a>`;    
            
        }
        return `<div class="nav-bar">
            ${subLinks}
        </div>`;
        
    }

    return `<div class="loe-top-nav">
    <div class="mobile-menu-spacer"></div>
    <div class="mobile-menu">
        <div class="top-bar">
            <input type="checkbox" class="menu-toggle" aria-label="menu toggle"/>
            <div class="linus"><div></div></div>
            <a href="https://www.logicofenglish.com" class="mobile-logo" aria-label="Logic of English - Home">
                <svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 33.89 15.05" class="breve">
                    <path d="M17.72,15.05C10,15.05,3.65,10.72,0,3.44.25,2.05,1.86-.15,2.85,0c3.78,5.48,8.55,8,14,8A17.05,17.05,0,0,0,31,0a6,6,0,0,1,2.85,4.5C32,9.41,25.27,15.05,17.78,15.05Z"></path>
                </svg>
            </a>
            <div class="menu-holder">${ entireNav(subMenu)}</div>
        </div>
    </div>    
    <a class="logo" href="https://www.logicofenglish.com">
        <img alt="Logic of English" src="https://res.cloudinary.com/logic-of-english/image/upload/LogicOfEnglish.svg" />
    </a>
    <span class="parent-link">
        <a href="${subMenu.url}">${subMenu.title}</a>
    </span>
    <div class="loe-search-holder"></div>
    <button type="button" class="shopping-cart" aria-label="shopping cart">
        <svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 24.05 20.45">                        
            <path d="M8.41,13.08c.41,1.37.71,1.6,2,1.6h10c.17,0,.38-.06.51,0a3.62,3.62,0,0,1,.57.54c-.19.16-.37.43-.58.45a17.78,17.78,0,0,1-1.79,0c-3,0-5.92,0-8.88,0a2.41,2.41,0,0,1-2.6-1.79c-.5-1.6-.9-3.24-1.32-4.88C5.75,7,5.27,5,4.74,2.93a6.27,6.27,0,0,0-.46-1.18A1.23,1.23,0,0,0,3.08,1C2.28,1,1.49,1,.69,1,.31,1-.08.93,0,.47.05.28.44.05.69,0,1.48,0,2.28,0,3.07,0A2.13,2.13,0,0,1,5.18,1.26a5.49,5.49,0,0,1,.55,1.46c.12.74.43,1,1.22,1,5.3,0,10.6,0,15.9,0,1.29,0,1.36.08,1,1.3-.6,2.43-1.22,4.86-1.8,7.3a.92.92,0,0,1-1.08.82c-3.93,0-7.86,0-11.79,0ZM8.08,12a.69.69,0,0,0,.21.06c4.1,0,8.21,0,12.31,0,.45,0,.52-.25.61-.59C21.6,9.9,22,8.28,22.4,6.67l.49-1.95H6.25Z"></path>
            <path d="M10.45,16.28a2.08,2.08,0,0,1,2.09,2.06,2.12,2.12,0,0,1-2.14,2.11,2.09,2.09,0,0,1,0-4.17Zm0,1a1,1,0,0,0-1,1.06,1,1,0,1,0,2.09-.06A1,1,0,0,0,10.42,17.31Z"></path>
            <path d="M18.84,16.29a2,2,0,0,1,2.08,2.07,2.07,2.07,0,0,1-2.08,2.09,2.12,2.12,0,0,1-2.09-2.1A2.06,2.06,0,0,1,18.84,16.29Zm1.05,2.07a1,1,0,0,0-1-1.05,1.08,1.08,0,0,0-1.09,1,1.11,1.11,0,0,0,1.07,1.11A1.08,1.08,0,0,0,19.89,18.36Z"></path>     
        </svg>
    </button>
</div>
${mainNav(subMenu)}
`;
}