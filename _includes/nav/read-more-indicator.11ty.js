const SVG = require('../svg/svg.11ty-filter.js');
module.exports = function() {

    return `<div class="read-more">Read more <div class="more-indicator">
    ${SVG('carrat')}${SVG('carrat')}${SVG('carrat')}
    </div></div>`
}
