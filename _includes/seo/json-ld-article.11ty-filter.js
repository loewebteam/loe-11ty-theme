module.exports = function({title="undefined",image="undefined",author="undefined",keywords="undefined",url="undefined",published="undefined",created="undefined",modified="undefined",description="undefined"}) {
    return `<script type="application/ld+json">
    { "@context": "http://schema.org", 
     "@type": "Article",
     "headline": "${title}",
     "image": "${image}",
     "author": "${author}",
     "genre": "Language Arts", 
     "keywords": "${keywords.join(', ')}", 
     "publisher": "Logic of English, Inc.",
     "url": "${url}",
     "datePublished": "${published}",
     "dateCreated": "${created}",
     "dateModified": "${modified}",
     "description": "${description}"
     }
    </script>`
}
