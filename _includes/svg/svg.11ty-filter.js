const svgs = {
    "carrat": `<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 24.24 12.8"><polygon class="cls-1" points="24.24 0.98 23.28 0 12.12 10.88 0.96 0 0 0.98 12.12 12.8 24.24 0.98"/></svg>`,
    "download-icon": `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144 144"><path d="M72.42,30.15A42.15,42.15,0,1,0,114.56,72.3,42.14,42.14,0,0,0,72.42,30.15ZM68.19,45.73h8.46v24.7h9.42L72.42,87.72,58.77,70.43h9.42ZM94.9,95h-45V80.41h2.25V92.22H92.65V80.41H94.9Z"/><path d="M72.42,30.15A42.15,42.15,0,1,0,114.56,72.3,42.14,42.14,0,0,0,72.42,30.15ZM68.19,45.73h8.46v24.7h9.42L72.42,87.72,58.77,70.43h9.42ZM94.9,95h-45V80.41h2.25V92.22H92.65V80.41H94.9Z"/></svg>`
}

module.exports = function(svg) {
    
    if(svg in svgs) {
        return svgs[svg];
    } else {
        return `<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 24.24 12.8"><text> MISSING SVG ${svg}</text></svg>`;
    }
    
}