const footer = require("../_includes/footer/footer.11ty");
const nodeSass = require("node-sass");
const topNav = require('loe-11ty-theme/_includes/nav/nav.11ty');

const fs = require('fs');
const path = require('path');

let inlineCSS = path.resolve(__dirname,'../sass/inline.scss');

if(fs.existsSync(inlineCSS)) {
    //custom inline css exists
} else {
    inlineCSS = path.resolve(__dirname,'../../node_modules/loe-11ty-theme/_sass/inline.scss')
}

inlineCSS = nodeSass.renderSync({
    file: inlineCSS,
    includePaths: [
        path.resolve(__dirname,'../../src/_sass'),
        path.resolve(__dirname,'../../node_modules/loe-11ty-theme/_sass/')
    ],
    outputStyle:'compressed'
})

class loeDefault {
    
    render(data) {
    let SEO = ``;
    let fbOGMetadata = ``;
    let socialImage = 'https://res.cloudinary.com/logic-of-english/image/upload/q_auto,w_360,h_360,c_pad,b_white/LOE-logo.jpg';

    let keywords = [];
    if(data.keywords) {
        if(typeof(data.keywords) == "string") {
            keywords = data.keywords.split(",");
        } else {
            keywords = data.keywords;
        }
    }

    keywords = keywords.map((keyword) => {
        return keyword.trim();
    });

    if(data.image) {
        socialImage = data.image;
    }

    if(data.includeSEO) {
        SEO = `
            <meta name="description" content="${data.description}">
            <meta name="keywords" content="${keywords.join(', ')}">`;

        fbOGMetadata = `        
        <meta property="og:url" content="${data.site.baseUrl}${data.page.url}" />
        <meta property="og:type" content="Web Page" />
        <meta property="og:title" content="${data.title}" />
        <meta property="og:description" content="${data.description}" />
        <meta property="og:image" content="${socialImage}" />`;
    }

    let inlineStyle = `<style media="screen">${inlineCSS.css}</style>`;

    let widgetArray = [
        'loe-cross-site-search',
        'loe-lazy-load',
        'loe-nav-map',
        'loe-contact-form'
    ];

    if(data.loeJsWidgets) {
        widgetArray = widgetArray.concat(data.loeJsWidgets);
    }

    let widgetString = "'" + widgetArray.join("', '") + "'";

    

    return `
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/x-icon" href="https://assets.logicofenglish.com/images/favicon.ico"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>${data.title}</title>
    ${SEO}
    ${fbOGMetadata}

    ${inlineStyle}

    <script type="application/ld+json">
    { "@context": "http://schema.org", 
    "@type": "WebPage",
    "image": "${socialImage}",
    "keywords": "${keywords.join(', ')}", 
    "publisher": "Logic of English, Inc.",
    "url": "${data.site.baseUrl}${data.page.url}}",
    "description": "${data.description}"
    }
    </script>

    <link rel="stylesheet" href="/assets/styles/common.css" media="none" onload="this.media='screen'"/>
    <noscript><link rel="stylesheet" href="/assets/styles/common.css"/></noscript>

    ${ data.css ? `<link rel="stylesheet" href="/assets/styles/${data.css}" media="none" onload="this.media='screen'"/>
    <noscript><link rel="stylesheet" href="/assets/styles/${data.css}"/></noscript>` : ''}
</head>
<body>
    <div class="top-nav">${topNav(data)}</div>
    <div class="content">${data.content}</div>   
    ${footer()}
    <div style="display:none" loe-11ty-theme="1.3.4"></div>
</body>
<script src="https://assets.logicofenglish.com/js/loe-common-js.js"></script>
<script>

    (function() {
    loeAsyncLoader({assetHost:'https://assets.logicofenglish.com/js/',
    jsObjList:[${widgetString}],
    cb:function() {
        LoeSearch.init({searchContainer:'div.loe-search-holder',resultTop:52});
        LoeNav.init({mapContainer:'div.menu-holder'});
        LoeNav.makeActive('${data.site.baseUrl}${data.page.url == "/" ? '' : data.page.url}');
        LazyLoad.loadResources({selector:'[data-src]',dataAttribute:'data-src',targetAttribute:'src'});   

        ${widgetArray.indexOf('loe-buy-now') == -1 ? '' : `LoeBuyNowBtn.init({selector:'[loe-buy-now-btn]',cartButtonSelector:'.loe-top-nav button.shopping-cart',cookieDomain:'.logicofenglish.com'});` }
        ${widgetArray.indexOf('loe-scroller') == -1 ? '' : `LoeScroller.init({selector:'[loe-scroller]'});`}
        ${widgetArray.indexOf('loe-expandi-box') == -1 ? '' : `ExpandiBox.init({selector:'[expandi]'});`}

        LoeContactForm.init({selector:'[loe-contact-btn]',settings:{
            contactForm: {
                tags:["site-{{site.data.sitemeta.title}}"]
            }
        }});

        ${!data.js ? '' : `loeAsyncLoader({assetHost:"/assets/scripts/",jsObjList:['${data.js.replace(".js","")}'],cb:function(){}});`}

    }});
    
})();
    
</script>

</html>
    `};
}

module.exports = loeDefault;

