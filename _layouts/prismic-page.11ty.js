const titleBlock = require('../_includes/misc-blocks/title-block.11ty-filter')
const faqs = require('../_includes/misc-blocks/faq-block.11ty');
const prismicDom = require('prismic-dom');
const cloudRoot = 'https://res.cloudinary.com/logic-of-english';
const cloudImgRoot = cloudRoot + '/image/upload/';

class PrismicPage {

    data() {
        return {
            layout: 'default.11ty.js'
        }
    }
    linkResolver(doc) {
        return doc.uid;
    }

    constructor() {
        this.nodeTypes = {
            pre:function(preNode) {
                return `<pre>${JSON.stringify(preNode,null,4)}</pre>`;
            },

            getsid:function(node) {
                if('sid' in node['non-repeat']) {
                    return node['non-repeat'].sid.value;
                }
                return null;
            },

            getclassname:function(baseClass="undefined",node) {
                if(baseClass == "undefined") {
                    baseClass = "";
                }
                let sid = this.getsid(node)
                if(sid) {
                    baseClass += ` ${sid}`;
                }
                return baseClass;
            },
        
            "hero_text":function(heroNode) {
                let className = this.getclassname("hero-text",heroNode);
                return `<div class="${className}">${prismicDom.RichText.asHtml(heroNode['non-repeat'].text.value)}</div>`
            },
        
            "section_header":function(headerNode) {
                let NOI = headerNode['non-repeat'].section_header.value;
                let className = this.getclassname("set-head",headerNode);
                return `<h3 class="${className}">${NOI}</h3>`;
            },
        
            text:function(textNode) {
                let className = this.getclassname("text-block",textNode);
                let NOI = textNode['non-repeat'].text.value;
                if(textNode.slice_label) {
                    className += ` ${textNode.slice_label}`;
                }
                return `<div class="${className}">${prismicDom.RichText.asHtml(NOI)}</div>`;
            },

            "image-text-widget":function(node) {
                let className = this.getclassname("image-text-widget",node);
                let textNode = node['non-repeat'].text.value;
                let imgNode = node['non-repeat'].cloudinary_image_name.value;
                let cssNode = node['non-repeat']['css-class'].value;
                if(cssNode) {
                    className += ` ${cssNode}`;
                }
                let transform = '';
                if('transformation_args' in node['non-repeat']) {
                    transform = `${node['non-repeat'].transformation_args.value}/`;
                }
                return `<div class="${className}">
                    <div class="img-holder">
                        <img data-src="${cloudImgRoot}${transform}${imgNode}" />
                    </div>
                    <div class="text">
                        ${prismicDom.RichText.asHtml(textNode)}
                    </div>
                </div>`
            },

            "cloudinary_image":function(node) {
                let className = this.getclassname("img-holder",node);
                let transform = '';
                if('transformation_args' in node['non-repeat']) {
                    transform = `${node['non-repeat'].transformation_args.value}/`;
                }
                let cloudImage = node['non-repeat'].cloudinary_image_title.value;

                return `<div class="${className}">
                    <img data-src="${cloudImgRoot}${transform}${cloudImage}" />
                </div>`;
            },

            "custom_html":function(node) {
                let className = this.getclassname("",node);
                return `<div class="${className}">
                ${node['non-repeat'].custom_html.value[0].text}
                </div>`;
            }
        }
    }

    sliceLoop(sliceArray) {
        
        return sliceArray.map((sliceNode) => {
            // switch(sliceNode.slice_type) {
            //     case 'hero_text':
            //         return this.heroText(sliceNode);
            //     case 'section_header'
            //     default:
            //         return this.pre(sliceNode);
            // } 
            if(sliceNode.slice_type in this.nodeTypes) {
                return this.nodeTypes[sliceNode.slice_type](sliceNode);
            }           
            return this.nodeTypes.pre(sliceNode);
        });
    }
    render(data) {
        const pageData = data[data.prismicId].data.pages;
        return `${titleBlock(pageData.page_title.value,pageData.page_tagline.value[0].text)}
        <div class="unstyled">
        ${this.sliceLoop(pageData.body.value).join('')}       
        ${faqs(data)}
        </div>`;
    }

}

module.exports = PrismicPage;