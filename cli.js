#!/usr/bin/env node

const [,, ...args] = process.argv;
const executeCommand = require('./src/LoeProcess');
const tempDir = require('./src/TemplateDirectory');
const watchLocal = require('./src/LocalTemplateWatch');
const prismicDocs = require('./src/PrismicDocs');

const cmdRequest = args[0];
const availableCommands = {
    "build": buildTempDirectory,
    "watch": watchLocal,
    "prismicdocs": getPrismicData,
    "hi": greet
}

function greet(args) {
    console.log("hi from loe-11ty-theme");
    console.log(...args);
}

async function getPrismicData(args) {
    //comma-delimited list of tags
    return prismicDocs(args[0]);
}

async function buildTempDirectory(args) {
    if(args && args.length == 1) {
        return tempDir({includes:args[0]})
    } else if (args && args.length == 2) {
        return tempDir({includes:args[0],layouts:args[1]})
    } else {
        return tempDir({})
    }
}

if(args.length == 0) {
    greet();
    return;
}

if(cmdRequest in availableCommands) {
    args.shift();
    availableCommands[cmdRequest](args);
} else {
    console.log("Available commands are as follows:");
    for(let cmd in availableCommands) {
        console.log(`     ${cmd}`);
    }
}

