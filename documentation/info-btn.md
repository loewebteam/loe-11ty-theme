
Info buttons are dependent on the common javascript included below the body tag in the layout

```yaml
loeJsWidgets:
    - loe-info-dialog
```

```html
    <span info-btn>Info buttons are dependent on the common javascript included below the body tag in the layout.</span>
```