.img-holders with img tags using a data-src attribute will lazy load.

```html
    <div class="img-holder">
        <img alt="" data-src="http://www.fillmurray.com/600/400">
    </div>
```