#misc-blocks/loe-asset-link.liquid

returns a string that is the link to an asset in assets.logicofenglish.com using the linkType and fileName properties.
- linkType should equal either "downloads" or "audio"
- fileName should be the name of the file (including extension).

```html
<a href="{% include misc-blocks/loe-asset-link.liquid linkType="downloads" fileName="fileName.pdf" %}" class="link-btn">Download</a>
```