loe-dialog is added to a page via the loeJsWidgets list, and then handed an array of dialog objects that tell loe-dialog which element opens the dialog, and what the contents of the dialog should be when that element is clicked/touched.

```yaml
js: page.js
loeJsWidgets:
    - loe-dialog
```


```html
    ....
    <div id="thing1">Click Me</div>
    <div id="thing2">Click Me Too</div>
```

```javascript
// page.js
(function(){
    //build the list of dialog button elements and dialog contents into an array in whatever way seems best.
    const dialogList = [
        {
            element:document.getElementById('thing1'),
            html:`<div>Here is some super, duper cool html</div>`
        },
        {
            element:document.getElementById('thing2'),
            html:`<div>This html is even cooler</div>`
        }
    ];
    //now initialize the LoeDialogButtons object - it'll talk to the loe-dialog when it needs to. Forget about it.
    LoeDialogButtons.init(dialogList);
})();

```
