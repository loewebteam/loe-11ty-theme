scroller div

# List

```yaml
list1:
    generalUrl: "https://logicofenglish.zendesk.com/hc/en-us"
    elements:
      - description: Here is a question?
        url: https://www.google.com
      - description: Here is another question?
        url: https://www.logicofenglish.com
loeJsWidgets:
    - loe-scroller
```

```html
    <div class="scrolly-holder">
        <div loe-scroller>
            <div class="scroll-strip" style="width:calc(100% * {{page.list1.elements.size}}">
                {% for frame in page.list1.elements %}
                <div class="scroller-frame">
                    <div class="img-holder">
                        <img alt="Bill Murray, a national treasure" data-src="https://www.fillmurray.com/200/200">
                    </div>
                    <div class="description">{{frame.description}}</div>
                    {%- if frame.url -%}
                    <div class="link-row" style="padding:1em 0">
                        <a href="{{frame.url}}" class="link-btn">Learn More</a>
                    </div>
                    {%- endif -%}
                </div>
                {% endfor %}
            </div>
        </div>
    </div>
```
# FAQs

```yaml
faqs:
    generalUrl: "https://logicofenglish.zendesk.com/hc/en-us"
    questions:
      - question: Here is a question?
        url: https://www.google.com
      - question: Here is another question?
        url: https://www.logicofenglish.com
loeJsWidgets:
    - loe-scroller
```

```html
    <div class="scrolly-holder">
        <div loe-scroller>
            <div class="scroll-strip" style="width:calc(100% * {{page.faqs.questions.size}}">
                {% for frame in page.faqs.questions %}
                <div class="scroller-frame">
                    <div class="img-holder">
                        <img alt="Bill Murray, a national treasure" data-src="https://www.fillmurray.com/200/200">
                    </div>
                    <div class="description">{{frame.question}}</div>
                    {%- if frame.url -%}
                    <div class="link-row" style="padding:1em 0">
                        <a href="{{frame.url}}" class="link-btn">Learn More</a>
                    </div>
                    {%- endif -%}
                </div>
                {% endfor %}
            </div>
        </div>
    </div>
```