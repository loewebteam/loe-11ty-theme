const minify = require('html-minifier').minify;
const minOptions = {
    collapseWhitespace:true,
    minifyJS:true,
    minifyCSS:true
}
const includesRoot = '../loe-11ty-theme/_includes/';

const tempDir = require('./src/TemplateDirectory');
const exec = require('./src/LoeProcess');


module.exports = {
    start:()=> {
        console.log('starting loe-11ty-theme');
    },
    minify:function(string,options="undefined") {
        if(options == "undefined") {
           options = minOptions; 
        }
        return minify(string,options);
    },
    miniTransform:function(content,outputPath) {
        if(outputPath.endsWith('.html')) {
            let minified = minify(content,minOptions);
            return minified;
        }

        return content;
    },
    registerLiquidFilters:function(eleventyConfig) {
        
        eleventyConfig.addLiquidFilter("faq",require(includesRoot + '/misc-blocks/faq-block.11ty.js'))
    },
    configureTheme:function(eleventyConfig="undefined",dir="undefined") {
        if(eleventyConfig == "undefined") {
            console.log("Must pass an eleventyConfig variable to configureTheme");
            return;
        }

        let tempDirs = {
            includes: '../_loe-11ty/_includes',
            layouts: '../_loe-11ty/_layouts'
        }

        let newDir = {
            input:"src",
            output: "public"
        }

        let buildObj = {
            includes:"src/_includes"
        }

        if(dir == "undefined") {
            newDir = Object.assign(newDir,tempDirs);
        } else {
            let projDir = process.cwd();
            let loeTempDir = "_loe-11ty";
            if(dir.includes) {
                buildObj["includes"] = dir.includes;
                let incFold = `${projDir}/${dir.includes}/`;
                eleventyConfig.addWatchTarget(`${projDir}/${loeTempDir}/_includes`);
            }
            if(dir.layouts) {
                buildObj["layouts"] = dir.layouts;
                let layFold = `${projDir}/${dir.layouts}/`;
                eleventyConfig.addWatchTarget(`${projDir}/${loeTempDir}/_layouts`);
            }
            newDir = Object.assign(dir,tempDirs);
        }
        this.build(buildObj);

        this.registerLiquidFilters(eleventyConfig)
        eleventyConfig.addTransform('miniTransform', this.miniTransform);

        return {
            dir:newDir
        }
    },
    build:tempDir,
    exec:function(cmd) {
        exec(cmd)
        .then((res) => {
            console.log(res);
            console.log('done');
        })
        .catch((er) => {
            console.log(er);
            console.log('error');
        })
    }
}