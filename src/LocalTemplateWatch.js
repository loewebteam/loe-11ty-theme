const fs = require('fs');
const watch = require('watch');
const projDir = process.cwd();
const loeTempDir = "_loe-11ty";
const watchMapPath = `${projDir}/${loeTempDir}/loe-11ty-map.json`;

function relLoc(file,from,to) {
    return file.replace(from,to);
}
function copySuccess(err) {
    if(err) {
        console.log(err);
        return;
    }
    console.log('file copied');    
}

function deleteSuccess(err) {
    if(err) {
        console.log(err);
    }
    console.log('file removed');
}

module.exports = function() {
    let watchMap = JSON.parse(fs.readFileSync(watchMapPath,'utf8'));
    console.log("watching...");
    let watchArray = watchMap.map((pair) => {
        return watch.createMonitor(pair.from, {interval:.2}, (monitor) => {
            
            monitor.on("created", (f) => {
                fs.copyFile(f,relLoc(f,pair.from,pair.to),copySuccess);
            })
            monitor.on("changed", (f) => {
                fs.copyFile(f,relLoc(f,pair.from,pair.to),copySuccess);
            })
            monitor.on("removed", (f) => {
                fs.unlink(relLoc(f,pair.from,pair.to),deleteSuccess);
            })
        })
    })

    return Promise.all(watchArray);
}
