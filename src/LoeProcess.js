const {exec} = require('child_process');

async function executeCommand(cmd) {
    return new Promise((resolve,reject) => {
        exec(cmd,(error,stdout,stderr) => {
            if (error) {
                reject(`${error.message}`);
                return;
            }
            if(stderr) {
                console.error(stderr);
                resolve(stderr);
                return;
            }
            resolve(stdout);
        });
    });
}

module.exports = executeCommand;