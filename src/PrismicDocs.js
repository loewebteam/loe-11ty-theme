const Prismic = require('prismic-javascript');
require('dotenv').config();
const prismicToken = process.env.PRISMIC;
const fs = require('fs');
const util = require('util');
const writeFile = util.promisify(fs.writeFile);
const projDir = process.cwd();

function initApi(req){
    return Prismic.getApi('https://loe-microsites.cdn.prismic.io/api', {
      accessToken: prismicToken,
      req: req
    });
  }

function savePage(page) {
  return writeFile(`${projDir}/src/_data/${page.uid}.json`,JSON.stringify(page,null,4),'utf8');
}

module.exports = async function(tags="undefined") {
  let allTags = ['all'];
  if (tags != "undefined") {
    tags = tags.split(",");
    allTags = allTags.concat(tags);
  }
  return initApi(``)
    .then((api) => {
        return api.query(
          Prismic.Predicates.any('document.tags', allTags)
        );
    })
    .then((ret) => {
      if('results' in ret) {
        let promArray = ret.results.map(savePage);
        return Promise.all(promArray);
      }
    })
    .then(() => {
      console.log('Prismic Data Saved');
    })
    .catch((er) => {
        console.log(er);
    });
}