const rimraf = require('rimraf');
const executeCommand = require('./LoeProcess');
const fs = require('fs');
const path = require('path');
const themeSrcDir = path.resolve(__dirname,'../');
const projDir = process.cwd();
const loeTempDir = "_loe-11ty";

module.exports = async function({includes="src/_includes",layouts="src/_layouts"}) {

    async function rmTempDir() {//clear out existing template directory
        return new Promise((resolve,reject) => {
            rimraf(`${projDir}/${loeTempDir}/**`,function(err) {
                if(err) {
                    reject(err);
                }
                resolve();
            }); 
        })
    }

    if(fs.existsSync(`${projDir}/${loeTempDir}`)) {
        console.log(`temp directory exists! Sterilizing.`);
        try {
            await rmTempDir();
        } catch(err) {
            console.error(err);
            return;
        }              
    }

    try {
        await executeCommand(`mkdir "${projDir}/${loeTempDir}"`);
        
    } catch(err) {
        console.error(err);
    }

    //create the directory update map to be used for watch command
    const watchMap = [
        {   
            theme:`${themeSrcDir}/_includes`,
            from:`${projDir}/${includes}`,
            to:`${projDir}/${loeTempDir}/_includes`
        },
        {
            theme:`${themeSrcDir}/_layouts`,
            from:`${projDir}/${layouts}`,
            to:`${projDir}/${loeTempDir}/_layouts`
        }
    ];
    fs.writeFileSync(`${projDir}/${loeTempDir}/loe-11ty-map.json`,JSON.stringify(watchMap),'utf8');

    let thememap = watchMap.map((dir) => {
        return executeCommand(`cp -r "${dir.theme}" "${dir.to}"`);
    })

    Promise.all(thememap)
    .then(() => {//copy from project/src/_** include and layout dirs into template directory
        let projmap = watchMap.map((dir) => {
            return executeCommand(`cp -r "${dir.from}/" "${dir.to}/"`)
        })
        return Promise.all(projmap);
    })
    .then(() => {
        console.log('done');
    })
    .catch((er) => {
        console.error(er);
    })


}